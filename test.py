from src.rembg.bg import remove
import numpy as np
import io
from PIL import Image

input_path = '/media/nivratti/programming/python/projects/rembg/examples/cassava_1000201771.jpg'
output_path = 'out.png'

cutout = remove(input_path)
cutout.save(output_path, alpha_matting=True)